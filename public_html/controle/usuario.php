<?php

include_once '../classes/conecta2.php';
include_once '../classes/usuario.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';


$usuario = new Usuario();


$usuario->setNome($_GET["id_user"]);

$filtro = ['nome' => $_GET["id_user"]];

$filtro2 = [NULL];
$projecao = ['nome' => 1, 'sobrenome' => 1, 'dt_nasc' => 1, 'cpf' => 1, 'email' => 1];


if ($usuario->getNome() == NULL) {
    $consulta = $filtro2;
} else {
    $consulta = $filtro;
}


$user = new Conectar();

$user->setconex();

$user->setCon($consulta, $projecao);
$user->setBaseCons('livraria.usuario');



$b = new Template("../templates/cadastrousuario.html");

foreach ($user->conecta() as $p) {
    $b->nome = $p->nome;
    $b->sobrenome = $p->sobrenome;
    $b->cpf = $p->cpf;
    $b->dt_nasc = $p->dt_nasc;
    $b->email = $p->email;

    $b->block("block_tabela");
}
$b->show();


include_once '../templates/redesociais.html';

include_once '../templates/foot.html';
