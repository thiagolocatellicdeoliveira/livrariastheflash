<?php

include_once '../classes/conecta2.php';
include_once '../classes/produto.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';




session_start(); // Inicia a sessão
if ($_SESSION['carrinho']) {

    unset($_SESSION['carrinho']);
    include_once '../templates/mensagem.html';
} else {

    header("location:index.php");
    echo("Não tem nada");
}


include_once '../templates/menulateral.html';

include_once '../templates/redesociais.html';


include_once '../templates/foot.html';
header("refresh: 5;index.php");
