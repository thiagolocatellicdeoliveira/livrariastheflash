<?php

include_once '../classes/conecta2.php';
include_once '../classes/noticia.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';

$ip = new Noticia();


$ip->setDescricao((filter_input(\INPUT_POST, 'descricao')));
$ip->setAutor((filter_input(\INPUT_POST, 'autor')));
$ip->setTitulo((filter_input(\INPUT_POST, 'titulo')));
$ip->setImagem((filter_input(\INPUT_POST, 'imagem')));


if ($ip->getTitulo() or $ip->getAutor() or $ip->getImagem()) {
    $insep = new Conectar();

    $insep->setconex();
    $insep->setCon($ip->insereNoticia());
    $insep->setBaseCons('livraria.noticia');
    $insep->insere();

    include_once '../templates/mensagem.html';
    include_once '../templates/menulateral.html';

    include_once '../templates/redesociais.html';
    header("refresh: 5;index.php");
} else {
    $insp = new Template("../templates/ins_noticia.html");
    $insp->show();
}


include_once '../templates/foot.html';

