<?php

include_once '../classes/conecta2.php';
include_once '../classes/usuario.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';

$ip = new usuario();


$ip->setNome((filter_input(\INPUT_POST, 'nome')));
$ip->setSobrenome((filter_input(\INPUT_POST, 'sobrenome')));
$ip->setEmail((filter_input(\INPUT_POST, 'email')));
$ip->setDt_nasc((filter_input(\INPUT_POST, 'dt_nasc')));
$ip->setCpf((filter_input(\INPUT_POST, 'cpf')));
$ip->setSenha(md5(filter_input(\INPUT_POST, 'senha')));
if ($ip->getSenha() and $ip->getEmail()) {
    $user = new Conectar();
    $user->setconex();

    $user->setCon($ip->insereUsuario());
    $user->setBaseCons('livraria.usuario');
    $user->insere();
    include_once '../templates/mensagem.html';
    include_once '../templates/menulateral.html';

    include_once '../templates/redesociais.html';
    header("refresh: 5;index.php");
} else {
    if ($ip->getDt_nasc() or $ip->getNome() or $ip->getCpf()) {
        include_once '../templates/mensagemerror.html';
        include_once '../templates/menulateral.html';

        include_once '../templates/redesociais.html';
    } else {

        $user = new Template("../templates/ins_usuario.html");
        $user->show();
    }
}

//echo($ip->getCpf());
//echo($ip->getNome());
//echo($ip->getEmail());
//echo($ip->getDt_nasc());
//echo($ip->getSenha());
//echo($ip->getSobrenome());




include_once '../templates/foot.html';

