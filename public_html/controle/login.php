<?php

include_once '../classes/conecta2.php';
include_once '../classes/usuario.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';



session_start(); // Inicia a sessão
if (!$_SESSION['login']) {
    $user = New Usuario();
    $user->setEmail((filter_input(\INPUT_POST, 'email')));
    $user->setSenha((filter_input(\INPUT_POST, 'password')));
    if ($user->getEmail() == NULL) {
        $b = new Template("../templates/login.html");
        $b->show();
    } else {

        $projecao = ['nome' => 1, 'autor' => 1, 'descricao' => 1, 'imagem' => 1, 'imagem2' => 1, 'categoria' => 1, 'editora' => 1, '_id' => 1];
        $filtro = ['email' => $user->getEmail(), 'senha' => md5($user->getSenha())];
        $consulta = $filtro;
        $prod = new Conectar();
        $prod->setconex();
        $prod->setCon($consulta, $projecao);
        $prod->setBaseCons('livraria.usuario');
        foreach ($prod->conecta() as $p) {
            session_start();
            $_SESSION['login'] = $p->email;
        }

        header("location:index.php");
    }
} else {

    header("location:index.php");
}


include_once '../templates/redesociais.html';

include_once '../templates/foot.html';
