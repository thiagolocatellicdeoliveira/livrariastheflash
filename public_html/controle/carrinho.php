<?php

include_once '../classes/conecta2.php';
include_once '../classes/produto.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';



session_start(); // Inicia a sessão

if ($_GET["id"] != NULL) {

    unset($_SESSION['carrinho']);
    $produto = new produto();
    $produto->setId_prod($_GET["id"]);

    $_SESSION['carrinho'] = $produto->getId();


    $projecao = ['nome' => 1, 'autor' => 1, 'descricao' => 1, 'imagem' => 1, 'imagem2' => 1, 'categoria' => 1, 'editora' => 1, '_id' => 1];
    $filtro = ['_id' => new MongoDB\BSON\ObjectId($_SESSION['carrinho'])];
    $consulta = $filtro;


    $prod = new Conectar();

    $prod->setconex();
    $prod->setCon($consulta, $projecao);
    $prod->setBaseCons('livraria.produto');



    $b = new Template("../templates/carrinhoCompras.html");

    foreach ($prod->conecta() as $p) {

        $b->nome = $p->nome;
        $b->imagem = $p->imagem;
        $b->descricao = $p->descricao;
        $b->preco = $p->preco;

        $b->block("block_tabela");
    }
    $b->show();
} else {

    header("location:index.php");
}


include_once '../templates/foot.html';
