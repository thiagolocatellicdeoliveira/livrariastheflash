<?php

include_once '../classes/conecta2.php';
include_once '../classes/produto.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';


$ip = new produto();

$ip->setNome_prod((filter_input(\INPUT_POST, 'nome_prod')));
$ip->setDesc_prod((filter_input(\INPUT_POST, 'desc_prod')));
$ip->setAutor_prod((filter_input(\INPUT_POST, 'autor_prod')));
$ip->setEdicao((filter_input(\INPUT_POST, 'edicao')));
$ip->setEditora((filter_input(\INPUT_POST, 'editora')));
$ip->setCategoria((filter_input(\INPUT_POST, 'categoria')));
$ip->setPreco((filter_input(\INPUT_POST, 'preco')));
$ip->setImagem((filter_input(\INPUT_POST, 'imagem')));
$ip->setImagem2((filter_input(\INPUT_POST, 'imagem2')));



if ($ip->getImagem() or $ip->getAutor() or $ip->getEditora() or $ip->getEdicao() or $ip->getCategoria() or $ip->getDescricao() or $ip->getImagem2()) {
    $insep = new Conectar();

    $insep->setconex();

    $insep->setCon($ip->insereProduto());
    $insep->setBaseCons('livraria.produto');
    $insep->insere();
    include_once '../templates/mensagem.html';
    include_once '../templates/menulateral.html';

    include_once '../templates/redesociais.html';
    header("refresh: 5;index.php");
} else {

    $insp = new Template("../templates/ins_produto.html");
    $insp->show();
}


include_once '../templates/foot.html';

