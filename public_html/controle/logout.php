<?php

include_once '../classes/conecta2.php';
include_once '../classes/usuario.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once '../templates/top.html';



session_start(); // Inicia a sessão
if (!$_SESSION['login']) {
    
} else {

    unset($_SESSION['login']);

    header("location:index.php");
}


include_once '../templates/redesociais.html';

include_once '../templates/foot.html';
