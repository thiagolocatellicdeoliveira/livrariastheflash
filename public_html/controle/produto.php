<?php

include_once '../classes/conecta2.php';
include_once '../classes/produto.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';


$produto = new Produto();

$produto->setId_prod($_GET["id"]);
$produto->setCategoria($_GET["categoria"]);
$produto->setEditora($_GET["editora"]);


$produto->setNome_prod((filter_input(\INPUT_POST, 'busca')));



if ($produto->getId() != NULL) {

    $filtro = ['_id' => new MongoDB\BSON\ObjectId($produto->getId())];
}
if ($produto->getEditora() != NULL) {
    $filtro = ['editora' => $produto->getEditora()];
}
if ($produto->getCategoria() != NULL) {
    $filtro = ['categoria' => $produto->getCategoria()];
}

if ($produto->getNome() != NULL) {

    $filtro = ['nome' => $produto->getNome()];
}

$filtro2 = [NULL];

$projecao = ['nome' => 1, 'autor' => 1, 'descricao' => 1, 'imagem' => 1, 'imagem2' => 1, 'categoria' => 1, 'editora' => 1, '_id' => 1];


if ($produto->getId() == NULL and $produto->getCategoria() == NULL and $produto->getEditora() == NULL and $produto->getNome() == NULL) {

    $consulta = $filtro2;
} else {

    $consulta = $filtro;
}


$prod = new Conectar();
$prod->setconex();

$prod->setCon($consulta, $projecao);
$prod->setBaseCons('livraria.produto');


if ($produto->getId() == NULL) {
    $b = new Template("../templates/produto.html");
    foreach ($prod->conecta() as $p) {

        $b->nome = $p->nome;
        $b->autor = $p->autor;
        $b->imagem = $p->imagem;
        $b->imagem2 = $p->imagem2;
        $b->edicao = $p->edicao;
        $b->preco = $p->preco;
        $b->id = $p->_id;

        $b->block("block_tabela");
    }
    $b->show();
    include_once '../templates/menulateral.html';
} else {
//    echo("oioi");
    $b = new Template("../templates/detalhes.html");

    foreach ($prod->conecta() as $p) {
//        echo("ooo");
        $b->nome = $p->nome;
        $b->imagem = $p->imagem;
        $b->descricao = $p->descricao;
        $b->preco = $p->preco;
        $b->id = $p->_id;

        $b->block("block_tabela");
    }
    $b->show();
}

include_once '../templates/redesociais.html';
include_once '../templates/foot.html';
