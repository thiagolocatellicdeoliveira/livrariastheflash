<?php

include_once '../classes/conecta2.php';
include_once '../classes/produto.php';
require_once("../lib/raelgc/view/Template.php");

use raelgc\view\Template;

include_once 'top.php';

$produto = new produto();


$filtro = ['descricao' => $produto->getDescproduto()];
$filtro2 = [NULL];
$projecao = ['cod_produto' => 1, 'descricao' => 1, 'preco_unit' => 1, 'qtd_produto' => 1, '_id' => 0];


if ($produto->getDescproduto() == NULL) {
    $consulta = $filtro2;
} else {
    $consulta = $filtro;
}


$prods = new Conectar();

$prods->setconex();

$prods->setCon($consulta, $projecao);
$prods->setBaseCons('projeto.produtos');




$b = new Template("../templates/ap_produto.html");
foreach ($prods->conecta() as $p) {
    $b->codpd = $p->cod_produto;
    $b->despd = $p->descricao;
    $b->preunitpd = $p->preco_unit;
    $b->qtdproduto = $p->qtd_produto;
    $b->block("block_tabela");
}
$b->show();



include_once '../templates/foot.html';
