<?php

include_once '../templates/top.html';
session_start();

if ($_SESSION['login']) {

    include_once '../templates/menucompleto.html';
} else {
    include_once '../templates/menu.html';
}
?>