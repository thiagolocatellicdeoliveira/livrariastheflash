<?php

class Noticia {

    public $id;
    public $descricao;
    public $autor;
    public $titulo;
    public $imagem;

    public function getImagem() {
        return $this->imagem;
    }

    public function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getAutor() {
        return $this->autor;
    }

    public function setAutor($autor) {
        $this->autor = $autor;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function insereNoticia() {
        $con = [
//      
            'titulo' => $this->titulo,
            'autor' => $this->autor,
            'descricao' => $this->descricao,
            'imagem' => $this->imagem];
        return $con;
    }

}
