<?php

class Produto {

    public $id;
    public $nome;
    public $autor;
    public $descricao;
    public $edicao;
    public $preco;
    public $imagem;
    public $imagem2;
    public $categoria;
    public $editora;

    /**
     * @return mixed
     */
    public function getCategoria() {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getEditora() {
        return $this->editora;
    }

    /**
     * @param mixed $editora
     */
    public function setEditora($editora) {
        $this->editora = $editora;
    }

    /**
     * @return mixed
     */
    public function getImagem() {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     */
    public function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    /**
     * @return mixed
     */
    public function getImagem2() {
        return $this->imagem2;
    }

    /**
     * @param mixed $imagem2
     */
    public function setImagem2($imagem2) {
        $this->imagem2 = $imagem2;
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getAutor() {
        return $this->autor;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getEdicao() {
        return $this->edicao;
    }

    function getPreco() {
        return $this->preco;
    }

    function setId_prod($id_prod) {
        $this->id = $id_prod;
    }

    function setNome_prod($nome_prod) {
        $this->nome = $nome_prod;
    }

    function setAutor_prod($autor_prod) {
        $this->autor = $autor_prod;
    }

    function setDesc_prod($desc_prod) {
        $this->descricao = $desc_prod;
    }

    function setEdicao($edicao) {
        $this->edicao = $edicao;
    }

    function setPreco($preco) {
        $this->preco = $preco;
    }

    function insereProduto() {
        $con = ['nome' => $this->nome,
            'autor' => $this->autor, 'edicao' => $this->edicao, 'editora' => $this->editora, 'categoria' => $this->categoria,
            'descricao' => $this->descricao, 'preco' => $this->preco, 'imagem' => $this->imagem, 'imagem2' => $this->imagem2];
        return $con;
    }

}
