<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of produto
 *
 * @author hp
 */
class Usuario {

    public $id_user;
    public $nome;
    public $sobrenome;
    public $cpf;
    public $dt_nasc;
    public $email;
//    public $usuario;
    public $senha;

//    /**
//     * @return mixed
//     */
//    public function getUsuario()
//    {
//        return $this->usuario;
//    }
//
//    /**
//     * @param mixed $usuario
//     */
//    public function setUsuario($usuario)
//    {
//        $this->usuario = $usuario;
//    }

    /**
     * @return mixed
     */
    public function getSenha() {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha) {
        $this->senha = $senha;
    }

    function getId_user() {
        return $this->id_user;
    }

    function getNome() {
        return $this->nome;
    }

    function getSobrenome() {
        return $this->sobrenome;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getDt_nasc() {
        return $this->dt_nasc;
    }

    function getEmail() {
        return $this->email;
    }

    function setId_user($id_user) {
        $this->id_user = $id_user;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setSobrenome($sobrenome) {
        $this->sobrenome = $sobrenome;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setDt_nasc($dt_nasc) {
        $this->dt_nasc = $dt_nasc;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function insereUsuario() {
        $con = [
            'nome' => $this->nome, 'sobrenome' => $this->sobrenome,
            'dt_nasc' => $this->dt_nasc, 'cpf' => $this->cpf,
            'email' => $this->email, 'senha' => $this->senha];
        return $con;
    }

}
